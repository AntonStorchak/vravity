//
//  Moon.swift
//  Vravity
//
//  Created by Anton Storchak on 10/27/16.
//  Copyright © 2016 Anton Storchak. All rights reserved.
//

import SpriteKit

class Moon {
    
    
    var mass = 1.0
    var force = CGPoint.zero
    var velocity = CGPoint.zero
    var sprite : SKShapeNode
    
    
    init(sprite : SKShapeNode) {
        self.sprite = sprite
    }
    
    func updateVelocity(deltaTime : Double) {
        let acceleration = force / mass
        let deltaVelocity = acceleration / deltaTime
        velocity += deltaVelocity
        force = CGPoint.zero
    }
    
}

func / (point : CGPoint, a : Double) -> CGPoint {
    return CGPoint(x : point.x / CGFloat(a) , y : point.y / CGFloat(a))
}

func / (someCGFloat : CGFloat, someDouble : Double) -> CGFloat {
    return (someCGFloat / CGFloat(someDouble))
}


func * (point : CGPoint, a : Double) -> CGPoint {
    return CGPoint(x : point.x * CGFloat(a) , y : point.y * CGFloat(a))
}

func * (someDouble : Double, someCGFloat : CGFloat) -> CGFloat {
    return ( CGFloat(someDouble) * someCGFloat)
}

func + (a : CGPoint, b : CGPoint) -> CGPoint {
   return CGPoint(x: a.x + b.x, y: a.y + b.y)
}
func += (inout a : CGPoint, b : CGPoint) {
    a = a + b
}

func - (a : CGPoint, b : CGPoint) -> CGPoint {
    return CGPoint(x: a.x - b.x, y: a.y - b.y)
}

/*func min (a : CGSize, b: CGSize) -> Double {
    if a.height <= b.height  {
        return a.height as Float
    } else {
        return b.height as Float
    }
}*/

extension CGPoint {
    var sqrMagnitude : Double {
        return Double(x*x + y*y)
    }
    
    var magnitude : Double {
        return sqrt(sqrMagnitude)
    }
    
    var normalizedVector : CGPoint {
        return  self/magnitude
    }
    
}

