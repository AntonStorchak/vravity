//
//  GameScene.swift
//  Vravity
//
//  Created by Anton Storchak on 10/26/16.
//  Copyright (c) 2016 Anton Storchak. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var moons = [Moon]()
    var pastTime : CFTimeInterval?
    let G = 1.0
    let C7Texture = SKTexture(imageNamed: "Star7")
    let C10Texture = SKTexture(imageNamed: "Star10")
    let C25Texture = SKTexture(imageNamed: "Star25")
    let C50Texture = SKTexture(imageNamed: "Star50")
    let C75Texture = SKTexture(imageNamed: "Star75")
    let C100Texture = SKTexture(imageNamed: "Star100")
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        self.backgroundColor = UIColor.blackColor()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            createMoon(inPoint: location)
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        let deltaTime = currentTime - (pastTime ?? currentTime)
        pastTime = currentTime
        
        var i = 0
        while i < moons.count {
            if moons[i].mass == 0 {
                moons[i].sprite.removeFromParent()
                moons.removeAtIndex(i)
                i -= 1
            }
            i += 1
        }
        
        for moon in moons {
            if self.size.width - moon.sprite.position.x <= 4 {
               moon.sprite.position.x -= moon.sprite.position.x     
            }
        }
        
        
        if moons.count > 1 {
            for i in 0 ..< (moons.count - 1) {
                for j in (i + 1) ..< moons.count {
                    let distanceVector = moons[j].sprite.position - moons[i].sprite.position
                    let sqrDistance = distanceVector.sqrMagnitude
                    let forceScalar = G * (moons[i].mass * moons[j].mass) / sqrDistance
                    let forceVector = distanceVector.normalizedVector * forceScalar
                    moons[i].force += forceVector
                    moons[j].force += forceVector * (-1)
                    
                    
                    
                    
                    if sqrt(sqrDistance) <= 10 /*min(moons[i].sprite.size , b: moons[j].sprite.size)*/ {
                        moons[i].mass += moons[j].mass
                        
                        //moons[i].sprite.fillColor = UIColor.blueColor()
                        //moons[i].sprite.glowWidth = 1
                        
                        
                        if (moons[i].mass >= 5 && moons[i].mass < 10) {
                            
                            moons[i].sprite.setScale(1.5)
                            moons[i].sprite.fillColor = UIColor.purpleColor()
                            moons[i].sprite.strokeColor = moons[i].sprite.fillColor
                            
                            
                        } else if (moons[i].mass >= 10 && moons[i].mass < 25) {
                            moons[i].sprite.setScale(3)
                            moons[i].sprite.fillColor = UIColor.blueColor()
                            moons[i].sprite.strokeColor = moons[i].sprite.fillColor
                        } else if (moons[i].mass >= 25 && moons[i].mass < 50) {
                            moons[i].sprite.setScale(5)
                            moons[i].sprite.fillColor = UIColor.blackColor()
                            moons[i].sprite.glowWidth = 0.05
                            moons[i].sprite.strokeColor = UIColor.whiteColor()
                            
                        } else if (moons[i].mass >= 50 && moons[i].mass < 75) {
                            moons[i].sprite.setScale(7)
                            moons[i].sprite.fillColor = UIColor.yellowColor()
                            moons[i].sprite.strokeColor = moons[i].sprite.fillColor
                        } else if (moons[i].mass >= 75 && moons[i].mass < 100) {
                            moons[i].sprite.setScale(8)
                            moons[i].sprite.fillColor = UIColor.orangeColor()
                            moons[i].sprite.strokeColor = moons[i].sprite.fillColor
                        } else if moons[i].mass >= 100 {
                            moons[i].sprite.setScale(9)
                            moons[i].sprite.fillColor = UIColor.blackColor()
                            moons[i].sprite.strokeColor = moons[i].sprite.fillColor
                        }
                        
                        moons[i].velocity.x = (moons[i].mass * moons[i].velocity.x + moons[j].mass * moons[j].velocity.x) / (moons[i].mass + moons[j].mass)
                        
                        
                        moons[i].velocity.y = (moons[i].mass * moons[i].velocity.y + moons[j].mass * moons[j].velocity.y) / (moons[i].mass + moons[j].mass)
                        
                        moons[j].mass = 0
                    }
                }
            }
        }
        
        for moon in moons {
            moon.sprite.position += moon.velocity * deltaTime
            moon.updateVelocity(deltaTime)
        }
        
    }
    
    func createMoon(inPoint point: CGPoint) {
        
        let roundMoon = SKShapeNode(circleOfRadius: 2.0)
        roundMoon.fillColor = UIColor.whiteColor()
        roundMoon.strokeColor = roundMoon.fillColor
        roundMoon.glowWidth = 1
        roundMoon.position = point
        addChild(roundMoon)
        
        
        /*let moonTexture = SKTexture(imageNamed: "moon")
        let moonSprite = SKSpriteNode(texture: moonTexture)
        addChild(moonSprite)
        moonSprite.position = point*/
        
        let moon = Moon(sprite: roundMoon)
        moons.append(moon)
    }
}
